+++
title = "Config file"
+++

For each host you connect to, you can specify a default name, user, port, and key file so that you don't have to type everything manually each time you connect.

```
Host example
    Hostname example.com
    User admin
    Port 2222
    IdentityFile ~/.ssh/example_key

Host gitlab
    Hostname gitlab.com
    User bikobi
    IdentityFile ~/.ssh/gitlab_key
```

With this configuration, simply launching `ssh example` will log you into `example.com` as "admin", using the correct port and key.

The `IdentityFile` field refers to the path of the *private* key you want to use. If "User" isn't specified, ssh will try to log in with the name of the local user.
