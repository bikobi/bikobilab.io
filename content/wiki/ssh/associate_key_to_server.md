+++
title = "Associate key to a server"
+++

To associate an SSH key to an SSH server, run

```bash
ssh-copy-id -i ~/.ssh/key.pub user@example.com
```

Answer the prompts and you're done.

Additional options:

- `-p` to specify the port;

