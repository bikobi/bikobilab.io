+++
title = "Generate keys"
+++

Generate a key with `ssh-keygen`. The flag `-t` specifies the type (rsa, ed25519, etc.). The flag `-C` specifies a comment.

```bash
ssh-keygen -t ed25519 -C "laptop"
```

You'll be prompted for the output file and an optional passphrase. The command will generate two new files (they should be in `~/.ssh/`) with the same name, but one with the `.pub` extension (the public key).
