+++
title = "Wiki"
template = "wiki.html"
page_template = "post.page.html"
+++

# About 

These pages are a collection of information and facts that I found useful over time.  

> I collected them here mostly for personal reference, and perhaps they may be useful to others as well; they're not intended as accurate tutorials.

Navigate the wiki via the menu, or use the searchbox.

The repo for this wiki is linked in the top-right icon. Feel free to report mistakes there.
