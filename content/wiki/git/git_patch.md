+++
title = "Commit only a portion of a file"
+++

```bash
git -p <filename>
```
