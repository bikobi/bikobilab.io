+++
title = "Use laptop as monitor"
+++

Sometimes I need to see the video output of the Raspberry Pi, but I don't have an extra monitor to connect. Instead, I use two cables:

- an HDMI-to-USB dongle (female HDMI, male USB);
- an HDMI-to-mini-HDMI cable (both males).

With these, connect the Raspberry Pi to a laptop, with the chain:

```
Raspberry Pi <- mini-HDMI -- HDMI -> dongle -> laptop
```

Then, on the laptop open VLC and select "Open capture device > TV - analog". 
Now turn on the Raspberry Pi: the video is displayed in VLC. You would still need to connect a keyboard for input.

