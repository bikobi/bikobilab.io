+++
title = "Hardware list"
+++

A checklist of the Raspberry Pi-related hardware that I own.

- [x] Raspberry Pi 4 Model B 4GB
- [x] plastic case
- [x] fan
- [x] microSD card
- [x] microSD card reader
- [x] power cable with a switch
- [x] ethernet cable
- [x] HDMI-to-mini-HDMI dongle

