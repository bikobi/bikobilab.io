+++
title = "All posts"
sort_by = "date"
template = "posts.html"
page_template = "post.page.html"
+++

Below is a list of my posts, most recent on top. You can subscribe to the **atom feed** from the navbar.  
The date is expressed in the format `%d-%m-%Y`.
