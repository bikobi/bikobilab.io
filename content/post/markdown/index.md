+++
title = "Markdown showcase"
date = 1970-01-01
+++

This page showcases various formatting features, mostly useful for testing.

## Text Styling

*Italic Text*

**Bold Text**

~~Strikethrough Text~~

`Monospace Text`

## Paragraphs and Newlines

This is a paragraph.

This is another paragraph.

## Headings

### Heading 3

#### Heading 4

##### Heading 5

###### Heading 6

## Footnotes

Here is some text with a footnote[^1].

## Blockquotes

> This is a blockquote.
>
> It can span multiple lines.

## Lists

### Ordered List

1. Item 1
2. Item 2
   1. Subitem 2.1
   2. Subitem 2.2
      - Subsubitem 2.2.1
      - Subsubitem 2.2.2
3. Item 3

### Unordered List

- Item A
- Item B
  - Subitem B.1
  - Subitem B.2
    - Subsubitem B.2.1
    - Subsubitem B.2.2
- Item C

## Links

[Example](https://www.example.com)

## Codeblocks

```python
# Some Python code:
def greet():
    print("Hello, world!") # Lines of code that are really long are NOT wrapped. Rather, the codeblock becomes horizontally scrollable.
```

## Images

![a placeholder image displaying the text "This is an image."](this_is_an_image.png)

## Tables

| Name  | Age | Location |
|-------|-----|----------|
| Alice | 30  | New York |
| Bob   | 35  | London   |


[^1]: This is the footnote.

