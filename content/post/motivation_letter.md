+++
title = 'Motivation letter for UT'
date = 2024-02-18T16:24:15+01:00
draft = true
+++

This is the motivation letter I wrote for admission to the University of Twente. It worked (I got admitted), so I figured I'd share it here as a resource for anyone needing to write their own, since I couldn't find any real-world examples when I needed them.

---

Dear Board of Admissions,

I appreciate the chance to apply to the University of Twente. I'm excited to share more about myself and explain why I'm keen on becoming part of the UT community.

My academic background reflects my long-standing interest in the technological field: I attended a school of applied sciences, in which math and physics are among the focus subjects, and I have never doubted my decision.

I'm fascinated by how simple concepts can be put together to form a complex yet elegant solution. One of my first exposures to the world of engineering was the discovery of the retractable ballpoint pen mechanism and, since then, spotting real-world appplications of concepts I had studied in physics class made the prospect of an engineering-related degree all the more appealing; I'm talking about eddy current separators and brakes, a car's steeringmechanism, differential circuit breakers.

It is clear to me that I want to pursue a career in engineering, and on my way there I intend to keep as many doors open as possible. Given its multidisciplinary nature, a broad programme like Advanced Technology is in my opinion perfect for this purpoise. At the same time, the elective modules will allow me to approach a field of specialissation as my intentions become clearer.

The fact that the programme is taught entirely in English only adds to it, as working with peers from diverse backgrounds is increasinngly commmon. Having studied abroad for a year, I am aware of the enormous advantage that this constitutes, both in the entrepeneurial landscape and in terms of personal growth. My yearning fo an international setting is, in fact, one of the many reasons that led me to prefer UT to other institutions.

Another reason is the breadth of interesting possibilities fo further my studies. The Master's programmes in Robotics and in Philosophy of Science, Technology, and Society particularly caught my attention: I myself dabble with hardware and software and have long followed the movements for free software and the right to repair. I also participate in the math, physics, and philosophy olympiads organised in my province.

Finally, the entrepeneurial and community-oriented mindset fostered at UT deeply resonates with me and fits my vision. I ultimately want to make the most of my years spent in higher education and nost just obtain a degree: I am eager to build  connections, to bring my enthusiasm, my dedication, and my skills to the table. The Advanced Technology programme is not just a pathway for my career in engineering; it's an opportunity to contribute to a community that values both technologucal innovation and the unique journey each student brings.

Hoping to soon be one of those students, I thank you very much for your time and interest;

bikobi
