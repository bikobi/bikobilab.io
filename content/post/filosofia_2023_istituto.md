+++
title = "On meekness (philosphy competition)"
date = 2024-01-23T08:00:00+01:00
+++

This essay was my submission for a local philosophy competition. Four possible topics were given, one for each of these philosophical areas: ethics, aesthetics, politics, and gnoseology. I picked ethics.  
The topics were assigned in italian; the essay was written in English. Below are the originals of both.

With this essay, I was admitted to the next stage of the competition.

**Disclaimer**: the essay does not necessarily reflect my views. I produced it in a limited timespan, for the sole purpose of the competition. It was not evaluated on its contents, but rather on its argumentative ability and philosophical references.

---

> *A maggior ragione la mitezzza è il contrario della prepotenza. Dico "a maggior ragione", perché la prepotenza è qualcosa di peggio rispetto alla protervia. La prepotenza è abuso di potenza non solo ostentata, ma concretamente esercitata. Il protervo fa bella mostra della sua potenza, del potere che ha di schiacciarti anche soltanto come si schiaccia con un dito una mosca o con un piede un verme. Il prepotente questa potenza la mette in atto, attraverso ogni sorta di abusi e soprusi, di atti di dominio arbitrario e, quando sia necessario, crudele. Il mite è invece colui che lascia essere l'altro quello che è, anche se l'altro è l'arrogante, il protervo, il prepotente. Non entra nel rapporto con gli altri con il proposito di gareggiare, di confliggere, e alla fine di vincere. È completamente al di fuori dello spirito della gara, della concorrenza, della rivalità, e quindi anche della vittoria. Nella lotta per la vita è infatti l'eterno sconfitto.*  
> &mdash; N. Bobbio, Elogio alla mitezza


In his *eulogy to meekness*, Bobbio defines a meek person as someone who *lets the other be what he is, even when the other is arrogant, insolent, or overbearing*. A eulogy being a praise, Bobbio maintains that meekness is a virtue.  
My thesis is that meekness is not a virtue and it is not a value upon which to base one's conduct.

Bobbio does not claim that the meek allows others to oppress him, but rather that he simply walks away from the competition, rejecting the idea of a "fight for life"; by Bobbio's own admission, however, the meek always ends up defeated in such fight.  
The fundamental problem lays here: as undesirable as it is, the fight for life does exist; it stems from the desire of some individuals to have power over others, be it for wealth, for pride, or by nature. And the fight for life involves everybody, because those who are determined to win it are willling to crush anyone else; so refusing to take part in it is not really an option, at least for those who do not wish to be crushed.

Yet, if that was all that there is to it, Bobbio's position would still be valid: one could accept to be crushed and oppose no resistance, while feeling virtuous for letting the other be what he is.

What Bobbio fails to consider is the responsibility of each individual to contribute to the improvement of society. Imagine if a parent let their child do anything, without intervening when he behaves inappropriately: with no one to correct him, the child would grow up to be disrespectful and harmful to others, possibly without even realizing it.  
The same duty that parents have towards their child, we all have towards those around us, or, to put it in other words, society has towards itself. By letting the arrogant be arrogant, the insolent be insolent, and  the overbearing be overbearing, the meek enforces the very system he despises.

Instead, one should oppose the other, when the other is being oppressive; at first by pointing out his wrongdoing and helping him change for the better; then, if there is no will to improve, the oppressor should be neutralized so that he can't harm anymore.

To be able to neutralize oppressors, either some individuals or institutions need to have some power over other parts of society, so there is the risk that they will in turn become oppressors; this problem is known as the *paradox of tolerance*: a society that aims to be as tolerant as possible must reserve the right to be intolerant with its intolerant members. This can be translated to an individual level: a person that wishes for the least amount of fighting will have to face some friction or contact precisely to put down the fight.

Drawing a conclusion, the most effective way to mitigate the despicable fight for life that many are forced into by a few is not to be meek, as asserted by Bobbio, but rather to act as referees and keep the others from beng bullyish and unfair.
