+++
title = "On modern democracies (philosphy competition)"
date = 2024-02-16T08:45:00+01:00
+++

This essay was my submission for a local philosophy competition. Four possible topics were given, one for each of these philosophical areas: ethics, aesthetics, politics, and gnoseology. I picked politics.  
The topics were assigned in italian; the essay was written in English. I wasn't able to retrieve the original text of the topic, therefore below is only my submission.

With this essay, I was *not* admitted to the next stage of the competition.

**Disclaimer**: the essay does not necessarily reflect my views. I produced it in a limited timespan, for the sole purpose of the competition. It was not evaluated on its contents, but rather on its argumentative ability and philosophical references.

---

In his Summa Theologica, Thomas Aquinas maintains that since the objective of the laws is the Common Good, i.e. what is good for the whole community, the law-making process should involve the whole community, or at most it should be reserved to someone who represents the whole community: only those who share the objective can work effectively toward it.

This statement seems to support modern democracies: the power belongs to the community, which exercises it either directly (for example in the form of referendums) or, most commonly, indirectly (by electing representatives), and this is supposed to serve the Common Good.

I believe that modern democratic systems do not serve the Common Good, but rather the good of the majority.

One fundamental and necessary observtion is that a community is made of individuals. Individuals often disagree, so the first problem that arises is: who gets to decide *what is the Common Good?*

Let's say there is a plot of land and the community must decide what is the best use for it. Someone wants to build a park, someone fancies a kindergarten, someone asks for a train station and someone else for parking lots.

The disagreement wouldn't necessarily stem from selfish citizens who put their own interests before those of the community; instead, it often comes from the different interpretations of the world that people have based on their experiences.  
In other words, a well-meaning citizen might genuinely believe that a parking lot will serve the community better than a kindergarten, because his *own* needs influence and distort his perception of the *collective* needs.

Modern democracies deal with the disagreement by trying to satisfy the greatest possible number of people, through a voting system: whichever preference has the most votes, wins. We're used to think of this as the best possible method, but it is heavily flawed.

Let's say there was a referendum to decide how to use the land from the previous example, with the same options, and these were the results:

- park: 40%;
- kindergarten: 30%;
- train station: 20%;
- parking lot: 10%.

A park would be built, and 40% of people would be satisfied. But satisfying 40% of people means disappointing the remaining 60%. Any other option would have disappointed more people, but we're still disappointing the absolute majority of the population.

In other cases, when the choice is binary, a consensus of at least 50% + 1 would be reached: for example if it was to be decided *whether or not* to build a park, the results could be 85% pro and 15% against: only a small fraction of the total population would be disappointed. But here arises another problem, that of oppression of minorities: in this case, for example, the opinion of 15% of the population is entirely disregarded. This can lead to great discrimination when the voting is about serious political matters.

These problems are mitigated in progressive voting systems, where for example a political party is assigned a fraction of the Parliament's seats corresponding to the polls result: 40% of votes would assign 40% of the seats.  
Such system simply *reduces* the room for oppression of the minorities, but in many cases it is not applicable, and it doesn't solve the fact that each party pursues an interpretation of the Common Good that differs from that of the other parties.

Another limitation of democracies is the assumption that just because something is desired by many people, it must be good, when actually there is no guarantee that quantity equals quality, as properly argued by Alexis De Tocqueville. The only way to tackle this without risking further discrimination is to guarantee a great level of education for all.

The issues of democratic systems are augmented when the community is very vast and it comprehends citizens from many cultural backgrounds: when this is the case, it becomes almost impossible to agree on a Common Good to pursue. One way to mitigate the disagreement would be to organize the decisional process into smaller, more local and more homogeneous communities, within which the interests of the individuals are more likely to coincide. In such context, it would be much easier to have an inclusive discourse and reach an agreed solution, and only resort to voting in extreme or pressing situations.
