+++
template = "index.html"
title = "Welcome"
+++

```
(\_/)   hello!
( ._.)
/>  >
```

I'm a {{ time_elapsed(timestamp=1064242800) }} year old student from Italy, studying engineering in the Netherlands.  

I do parkour, tinker with computers and I have an interest for philosophy.

## About this website

This static website is built with [Zola](https://getzola.org) and hosted on [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/). The source code is available on [GitLab](https://gitlab.com/bikobi/bikobi.gitlab.io).
