# Website

![pipeline](https://gitlab.com/bikobi/bikobi.gitlab.io/badges/main/pipeline.svg)

My personal website, built with [Zola](https://getzola.org).

Visit: [bikobi.gitlab.io](https://bikobi.gitlab.io).

- [x] Minimal interface
- [x] Responsive design
- [x] Automatic dark theme
- [x] Nested menu
- [x] 100% static (no JS, no bloat, no tracking)

